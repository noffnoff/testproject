FROM openjdk:12-alpine

ENV PORT 8080
EXPOSE $PORT

RUN addgroup -S app && adduser -S app -G app
USER app

WORKDIR /opt/app/
COPY ./build/libs/*.jar /opt/app/app.jar

RUN pwd && ls -lAa
CMD java -jar app.jar -Dserver.port=${PORT}
